#!/bin/sh
musl-gcc -c -I/musl/include test.c
musl-gcc -static -o test test.o -L /musl/lib/ -lpq
./test
